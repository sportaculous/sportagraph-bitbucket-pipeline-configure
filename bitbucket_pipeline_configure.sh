#!/usr/bin/env bash

ARTEFACTS_REPO_FQDN=$1
ARTEFACTS_REPO_FETCH_USER=$2
ARTEFACTS_REPO_FETCH_PASSWORD=$3

AWS_ACCESS_KEY=$4
AWS_SECRET_KEY=$5

CHEF_ENDPOINT=$6
CHEF_USER=$7
CHEF_USER_KEY=$8

CHEF_VALIDATION_USER=$9
CHEF_VALIDATION_KEY=$10

SBT_HOME=$HOME/.sbt

SBT_EXE_PATH=(`readlink -f \`which sbt\``)
SBT_OPTS_PATH=(`dirname \`dirname $SBT_EXE_PATH\``/conf/sbtopts)

mkdir -p ${SBT_HOME}/0.13

mkdir -p ${SBT_HOME}/0.13/plugins

rm ${SBT_HOME}/repositories
rm ${SBT_HOME}/credentials
rm ${SBT_HOME}/0.13/plugins/settings.sbt

# sbt settings
cat > $SBT_OPTS_PATH <<EOF
-J-XX:MaxMetaspaceSize=256M

-J-XX:+UseCodeCacheFlushing

-J-XX:ReservedCodeCacheSize=256M

-J-XX:+CMSClassUnloadingEnabled

-J-Xss1M

-J-Xms512M

-J-Xmx5G

-Dfile.encoding=UTF-8
EOF


# build settings
cat > ${SBT_HOME}/0.13/settings.sbt <<EOF
def sportaIvyRepo(name: String) = {
  Resolver.url("Sportagraph's Ivy '"+name+"' Repository", url("https://${ARTEFACTS_REPO_FQDN}/ivy/" + name + "/"))(Resolver.ivyStylePatterns)
}

def sportaMavenRepo(name: String) = {
  "Sportagraph's Maven '"+name+"' Repository" at ("https://${ARTEFACTS_REPO_FQDN}/maven/" + name + "/")
}

resolvers := Seq(
  Resolver.defaultLocal, 
  Resolver.mavenLocal
) ++ 
  List("private/releases", "private/snapshots", "public/releases", "public/snapshots").map(sportaIvyRepo) ++ 
  List("private/releases", "private/snapshots", "public/releases", "public/snapshots").map(sportaMavenRepo) ++ 
  Seq(
    "maven-central" at "https://repo1.maven.org/maven2/",
    "spray repo" at "http://repo.spray.io",  
    Resolver.url("typesafe-ivy-releases", url("https://repo.typesafe.com/typesafe/ivy-releases"))(Resolver.ivyStylePatterns),
    Resolver.url("scalasbt-plugins-releases", url("https://repo.scala-sbt.org/scalasbt/sbt-plugin-releases"))(Resolver.ivyStylePatterns),
    "jcenter" at "https://jcenter.bintray.com",
    "sonatype-releases" at "https://oss.sonatype.org/content/groups/public",
    "sonatype-snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
    "elastic-releases" at "https://artifacts.elastic.co/maven",
    "scalaz-releases" at "https://dl.bintray.com/scalaz/releases",
    "adobe-public" at "https://repo.adobe.com/nexus/content/repositories/public"
  )

// Used for publishing Sportagraph artefacts
credentials += Credentials("Sportagraph Publish Aws Realm", "${ARTEFACTS_REPO_FQDN}", "${AWS_ACCESS_KEY}", "${AWS_SECRET_KEY}")

// Used for fetching Sportagraph artefacts
credentials += Credentials("Sportagraph Realm", "${ARTEFACTS_REPO_FQDN}", "${ARTEFACTS_REPO_FETCH_USER}", "${ARTEFACTS_REPO_FETCH_PASSWORD}")

credentials += Credentials("Sportagraph Chef User Realm", "${CHEF_ENDPOINT}", "${CHEF_USER}", "${CHEF_USER_KEY}")

credentials += Credentials("Sportagraph Chef Organization Realm", "${CHEF_ENDPOINT}", "${CHEF_VALIDATION_USER}", "${CHEF_VALIDATION_KEY}")
EOF